import re
import datetime
import random
import string
import logging
import os
from hashlib import md5, sha1
from flask import Flask, request, Response
import psycopg2
import gnupg
from config import Configurator
from version import __version__


application = Flask(__name__)
if os.getenv("RUNMODE") == "production":
    application.logger.setLevel(logging.INFO)
else:
    application.logger.setLevel(logging.DEBUG)
# Loglevel can be overrinden by LOGLEVEL env var :
if os.getenv("DEBUG") == "true":
    application.logger.setLevel(logging.DEBUG)
application.config.from_object(Configurator)


def wsshash(login, password):
    """
    Compute a hash suitable for the IRIS wss stack.
    """
    return md5(("%s:FDSN:%s" % (login, password)).encode()).hexdigest()


def verify_token_signature(data, gpg_homedir):
    # First we verify the signature
    gpg = gnupg.GPG(gnupghome=gpg_homedir)
    verified = gpg.verify(data)
    if not verified:
        raise ValueError("Signature could not be verified!")
    return True


def parse_input_data(data):
    # Then we get the token :
    token = re.search(r"{(?P<token>.*)}", str(data)).groupdict()["token"]
    application.logger.debug(token)
    d = dict(
        [i for i in kv.split(":", 1)]
        for kv in token.replace('"', "").replace(" ", "").split(",")
    )
    # Add the sha1 of the token to put in the credentials db
    d["shortSha1"] = sha1(token.encode()).hexdigest()[0:8]
    if "givenName" not in d:
        d["givenName"] = d.get("cn", "null")
    application.logger.debug("Transformed to dictionary : %s", d)
    return d


def register_privileges(login, tokendict):
    """
    - Find all privileges defined in the authorization table (locally at Résif)
    - Find all privileges attached to the epos groups found in the token
    - Register the privileges in the database
    """
    authorized_network_ids = []

    # First get localy defined autorizations, from email adress
    # 1. Chercher tous les users ayant cet email dans la table resifAuth.users
    #    select login from users where email = '' and expires_at is NULL
    # 2. Pour chaque login récupéré, prendre la liste des réseaux autorisés dans resifInv
    #    select network_id, network, start_year, end_year from resif_users where name=%(login)s
    #    Ajouter ces tuples à membership pour que les autorisations soient accordées à cet utilisateur
    #
    #
    # Step 1 : Get the list of users related to the email adress in the token
    permanent_logins = []
    with psycopg2.connect(
        dbname=application.config["RESIFAUTH_PGDATABASE"],
        port=application.config["RESIFAUTH_PGPORT"],
        host=application.config["RESIFAUTH_PGHOST"],
        user=application.config["RESIFAUTH_PGUSER"],
        password=application.config["RESIFAUTH_PGPASSWORD"],
    ) as conn:
        cur = conn.cursor()
        application.logger.debug("Connected to users database")
        cur.execute(
            "select login from users where email=%s and expires_at is null",
            (tokendict["mail"],),
        )
        for l in cur:
            application.logger.debug(
                "Found an account corresponding to %s: %s", tokendict["mail"], l[0]
            )
            permanent_logins.append(l[0])

    # Now, connect to the inventory database
    try:
        conn = psycopg2.connect(
            dbname=application.config["RESIFINV_PGDATABASE"],
            port=application.config["RESIFINV_PGPORT"],
            host=application.config["RESIFINV_PGHOST"],
            user=application.config["RESIFINV_PGUSER"],
            password=application.config["RESIFINV_PGPASSWORD"],
        )
        cur = conn.cursor()
        application.logger.debug("Connected to resifinv database")
    except Exception as e:
        application.logger.error(
            "Unable to connect to database %s as %s@%s:%s",
            application.config["RESIFINV_PGDATABASE"],
            application.config["RESIFINV_PGUSER"],
            application.config["RESIFINV_PGHOST"],
            application.config["RESIFINV_PGPORT"],
        )
        raise e

    # Then, find out the mapping between EPOS naming and internal network reference
    # For instance "/epos/alparray" -> 34
    epos_names = tokendict["memberof"].split(";")
    application.logger.debug(
        "Mapping %s to network identifiers in inventory", epos_names
    )
    cur.execute(
        "select distinct network_id from epos_network_map where epos_name = any (%s)",
        (epos_names,),
    )
    for ref in cur:
        authorized_network_ids.append(ref[0])

    # Now for each permanent login attached to the token's email, get the localy defined privileges

    for l in permanent_logins:
        application.logger.debug("Searching for privileges for login %s", l)
        cur.execute(
            "select distinct network_id from resif_users where name=%s",
            (l,),
        )
        for ref in cur:
            authorized_network_ids.append(ref[0])

    if len(authorized_network_ids) == 0:
        application.logger.debug("No membership for user %s", login)
        return
    application.logger.debug(
        "User %s has privileges for networks ID %s", login, authorized_network_ids
    )

    # Finaly, we can register all authorized_network_ids
    # Iterate through unique net_id
    for net_id in list(dict.fromkeys(authorized_network_ids)):
        expires_at = datetime.datetime.now() + datetime.timedelta(days=1)
        application.logger.info(ref)
        application.logger.info(
            "Inserting tupple in %s.eida_temp_users: %s, %s, %s",
            application.config["RESIFINV_PGDATABASE"],
            login,
            expires_at,
            net_id,
        )
        cur.execute(
            """
        INSERT INTO eida_temp_users (network_id, name, expires_at)
        VALUES (%s, %s, %s)
        ON CONFLICT DO NOTHING;
        """,
            (net_id, login, expires_at),
        )
    conn.commit()
    conn.close()


def get_login_password(tokendict):
    """
    Try to read the token's mail. If it exists and a valid credential already exists in the database, return this tuple
    else, generate a new login/password, register it in the database and return the tupple
    """
    login = ""
    password = ""
    application.logger.debug(
        "Trying to connect to resifauth with %s %s %s %s",
        application.config["RESIFAUTH_PGDATABASE"],
        application.config["RESIFAUTH_PGPORT"],
        application.config["RESIFAUTH_PGHOST"],
        application.config["RESIFAUTH_PGUSER"],
    )
    try:
        conn = psycopg2.connect(
            dbname=application.config["RESIFAUTH_PGDATABASE"],
            port=application.config["RESIFAUTH_PGPORT"],
            host=application.config["RESIFAUTH_PGHOST"],
            user=application.config["RESIFAUTH_PGUSER"],
            password=application.config["RESIFAUTH_PGPASSWORD"],
        )
        cur = conn.cursor()
        application.logger.debug("Connected to users database")
        cur.execute(
            "select user_index,login from users where email=%s and expires_at between now()+'1 hour' and now()+'26 hours'",
            (tokendict["mail"],),
        )

        application.logger.debug(
            cur.mogrify(
                "select user_index,login from users where email=%s and expires_at between now()+'1 hour' and now()+'26 hours'",
                (tokendict["mail"],),
            )
        )
        if cur.rowcount != 0:
            (uid, login) = cur.fetchone()
            application.logger.debug(
                "Found a temporary account corresponding to %s: %s",
                tokendict["mail"],
                uid,
            )
            cur.execute(
                "select password_hash from credentials where user_index=%s", (uid,)
            )
            if cur.rowcount != 0:
                password = cur.fetchone()[0]
                application.logger.debug(
                    "Found correspondig password also: %s", password
                )
        # If not found, compute a random login and password
        if not login or not password:
            application.logger.debug("No existing user found. Create a new one")
            login = "".join(
                random.choices(string.ascii_uppercase + string.digits, k=14)
            )
            password = "".join(
                random.choices(string.ascii_uppercase + string.digits, k=14)
            )
            expiration_time = datetime.datetime.now() + datetime.timedelta(days=1)
            # Register login in authentication database
            cur.execute(
                """
                INSERT INTO users VALUES (DEFAULT, %(login)s, %(sn)s, %(givenName)s, %(mail)s, %(expires_at)s);
                """,
                {
                    "login": login,
                    "givenName": tokendict["givenName"],
                    "sn": tokendict["sn"],
                    "mail": tokendict["mail"],
                    "expires_at": expiration_time,
                },
            )
            cur.execute(
                """
            INSERT INTO credentials (user_index, password_hash, ws_hash, expires_at) VALUES (CURRVAL('users_user_index_seq'), %(password)s, %(wsshash)s, %(expires_at)s);
            """,
                {
                    "wsshash": wsshash(login, password),
                    "expires_at": expiration_time,
                    "password": password,
                },
            )
        conn.commit()
        conn.close()

    except psycopg2.Error as e:
        application.logger.error(e.pgerror)
        raise e

    return (login, password)


@application.route("/version", methods=["GET"])
def version():
    return Response(
        "Version %s running in %s mode. Contact %s."
        % (
            __version__,
            application.config["ENVIRONMENT"],
            application.config["SUPPORT_EMAIL"],
        ),
        status=200,
    )


@application.route("/cleanup", methods=["GET"])
def cleanup():
    """
    Clean old temporary logins and passwords in both databases.
    """
    application.logger.info("Cleaning up expired temporary accounts")
    rows_deleted = 0
    try:
        conn = psycopg2.connect(
            dbname=application.config["RESIFAUTH_PGDATABASE"],
            port=application.config["RESIFAUTH_PGPORT"],
            host=application.config["RESIFAUTH_PGHOST"],
            user=application.config["RESIFAUTH_PGUSER"],
            password=application.config["RESIFAUTH_PGPASSWORD"],
        )
        cur = conn.cursor()
        application.logger.debug("Connected to users database")
        cur.execute("delete from users where expires_at < now();")
        rows_deleted = cur.rowcount
        conn.commit()
        conn.close()
    except psycopg2.Error as e:
        application.logger.error(e.pgerror)
        raise e

    try:
        conn = psycopg2.connect(
            dbname=application.config["RESIFINV_PGDATABASE"],
            port=application.config["RESIFINV_PGPORT"],
            host=application.config["RESIFINV_PGHOST"],
            user=application.config["RESIFINV_PGUSER"],
            password=application.config["RESIFINV_PGPASSWORD"],
        )
        cur = conn.cursor()
        application.logger.debug("Connected to privlieges database")
        application.logger.debug("Deleting from privileges database")
        cur.execute("delete from eida_temp_users where expires_at < now();")
        conn.commit()
        conn.close()
    except Exception as e:
        application.logger.error(e.pgerror)
        raise e
    return Response("Deleted %d expired accounts." % (rows_deleted), status=200)


@application.route("/", methods=["POST"])
def auth():
    login = ""
    password = ""
    application.logger.debug(request.mimetype)
    data = request.get_data()
    application.logger.debug("Data: %s", data)
    try:
        verify_token_signature(data, application.config["GNUPG_HOMEDIR"])
        tokendict = parse_input_data(data)
        application.logger.info("Token signature OK: %s" % str(tokendict))
    except ValueError as e:
        application.logger.info("Token signature could not be checked: %s" % str(data))
        return Response(str(e), status=415)
    # Now we have a dictionary corresponding to the token's content.
    # Verify validity
    expiration_ts = datetime.datetime.strptime(
        tokendict["valid_until"], "%Y-%m-%dT%H:%M:%S.%fZ"
    )
    if (expiration_ts - datetime.datetime.now()).total_seconds() < 0:
        application.logger.info("Token is expired")
        return Response(
            "Token is expired. Please generate a new one at https://geofon.gfz-potsdam.de/eas/",
            status=400,
        )
    application.logger.info("Token is valid")

    (login, password) = get_login_password(tokendict)
    if login and password:
        register_privileges(login, tokendict)
        return "%s:%s" % (login, password)
    else:
        return Response(
            "Internal server error. Contact %s" % (application.config["SUPPORT_EMAIL"]),
            status=500,
        )


if __name__ == "__main__":
    application.logger.info("Running in %s mode" % (application.config["ENVIRONMENT"]))
    application.run(host="0.0.0.0")
